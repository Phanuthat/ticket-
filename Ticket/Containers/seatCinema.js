import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  FlatList,
  StyleSheet,
  TouchableWithoutFeedback,
  TouchableOpacity
} from "react-native";
import { Button, WhiteSpace, WingBlank } from "@ant-design/react-native";

import { connect } from "react-redux";

import seat1 from "../Img/chair.png";

class seatCinema extends Component {
    UNSAFE_componentWillMount(){
        console.log("this",this.props.location.state);
    }
  getInitialState = () => {
    return {
      seat1
    };
  };
  cancel=()=>{
    this.props.history.push("/main")
  }
  onClick = (item) => {
    this.props.history.push("/historyticket", { seat: item ,time:this.props.location.state});
  };
  handleClick = () => {
    this.setState({
      bgColor: "blue"
    });
  };
  state = {
    rows: [
      "1A",
      "2A",
      "3A",
      "4A",
      "5A",
      "6A",
      "7A",
      "8A",
      "1B",
      "2B",
      "3B",
      "4B",
      "5B",
      "6B",
      "7B",
      "8B",
      "1C",
      "2C",
      "3C",
      "4C",
      "5C",
      "6C",
      "7C",
      "8C"
    ]
  };
  render() {
    return (
      <View
        style={{ flex: 1, backgroundColor: "1E90FF", justifyContent: "center" }}
      >
        <Text style={styles.textProfile}>Cinema </Text>
        <View
          style={{
            flex: 1,
            backgroundColor: "1E90FF",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <FlatList
            data={this.state.rows}
            numColumns={8}
            renderItem={({ item }) => {
              return (
                <View
                  style={{
                    marginHorizontal: 5,
                    marginVertical: 10,
                    marginTop: 50,
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                >
                  <TouchableOpacity onPress={() => this.onClick(item)}>
                    <Image source={seat1} style={{ height: 30, width: 30 }} />
                  </TouchableOpacity>
                </View>
              );
            }}
          />
        </View>
        <View style={styles.row}>
          <View style={styles.buttonContainer}>
            <Button
              style={{ marginHorizontal: 10 }}
              type="primary"
              onPress={() => {
                this.onSave();
              }}
            >
              Checkout
            </Button>
            <Button
              type="warning"
              onPress={() => {
                this.cancel();
              }}
            >
              Cancel
            </Button>
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  textProfile: {
    // fontFamily: "Avenir",
    textAlign: "center",
    // fontSize: 24,
    // marginTop: 10,
    backgroundColor: "#1E90FF",
    padding: 15,
    color: "white",
    fontSize: 20,
    textAlign: "center"
  },
  buttonContainer: {
    marginHorizontal: 10,
    flexDirection: "row"
  },
  row: {
    flexDirection: "row",
    paddingVertical: 20,
    justifyContent:"center",
    textAlign:"center",
    marginTop:50
  },
});

const mapStateToProps = state => {
  return {
    user: state.userone
  };
};

export default connect(
  mapStateToProps,
  null
)(seatCinema);
