import React, { Component } from "react";
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  KeyboardAvoidingView,
  Keyboard,
  TouchableWithoutFeedback,
  TouchableOpacity
} from "react-native";

import axios from "axios";
import { connect } from "react-redux";

import { Button } from "@ant-design/react-native";

const DissmissKeyboard = ({ children }) => (
  <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
    {children}
  </TouchableWithoutFeedback>
);

class register extends Component {
  state = {
    email: "",
    password: "",
    firstname: "",
    lastname: ""
  };
  onLogin = () =>{ this.props.history.push("/Login")}
  onSignup = () => {
    axios
      .post("https://zenon.onthewifi.com/ticGo/users/register?email", {
        email: this.state.email,
        password: this.state.password,
        firstName: this.state.firstname,
        lastName: this.state.lastname
      })
      .then(response => {
        this.props.history.push("/Login");
        console.log("response", response.data);
        console.log("response ++", response);
      })
      .catch(error => {
        console.log("err", error.data);
      });
  };

  render() {
    return (
      <DissmissKeyboard>
        <KeyboardAvoidingView style={styles.container} behavior="padding">
          <View style={styles.container}>
            <View style={styles.logoContainer}>
              <Text style={styles.text}>Register</Text>
              <View>
                <TextInput
                  style={styles.inputText}
                  placeholder="Enter emai "
                  placeholderTextColor="gray"
                  onChangeText={email => this.setState({ email })}
                />

                <TextInput
                  style={styles.inputText}
                  placeholder="Enter password"
                  placeholderTextColor="gray"
                  secureTextEntry={true}
                  onChangeText={password => this.setState({ password })}
                />

                <TextInput
                  style={styles.inputText}
                  placeholder="Enter firstname"
                  placeholderTextColor="gray"
                  onChangeText={firstname => this.setState({ firstname })}
                />

                <TextInput
                  style={styles.inputText}
                  placeholder="Enter lastname"
                  placeholderTextColor="gray"
                  onChangeText={lastname => this.setState({ lastname })}
                />
              </View>
              <View style={styles.row}>
                <View style={styles.buttonContainer}>
                  <Button type="primary" onPress={() => this.onSignup()}>
                    Sign in
                  </Button>
                  
                </View>
                <View style={{justifyContent:"center"}}>
                    <TouchableOpacity onPress={()=>this.onLogin()}>
                      <Text style={{color:"#1E90FF" , textDecorationLine:"underline" , fontSize:16}}> or singup </Text>
                    </TouchableOpacity>
                  </View>
              </View>
            </View>
          </View>
        </KeyboardAvoidingView>
      </DissmissKeyboard>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "white"
  },
  text: {
    textAlign: "center",
    fontSize: 24,
    color: "#1E90FF"
  },
  logoContainer: {
    alignItems: "center",
    justifyContent: "center",
    flex: 1
  },
  logo: {
    width: 10,
    height: 50,
    borderRadius: 50
  },
  row: {
    flexDirection: "row",
    paddingVertical: 20
  },
  buttonContainer: {
    marginHorizontal: 10
  },
  inputText: {
    backgroundColor: "#DCDCDC",
    borderRadius: 15,
    height: 40,
    width: 300,
    paddingLeft: 45,
    marginTop: 20
  }
});

export default register;
