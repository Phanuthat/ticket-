import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  Image,
  TouchableHighlight
} from "react-native";
import { Button, WhiteSpace, WingBlank } from "@ant-design/react-native";
import ImagePicker from "react-native-image-picker";
import axios from "axios";

import user from "../Img/users.png";
import Menu from "./Menu";

import { connect } from "react-redux";
class profile extends Component {
  kut = () => {
    return ({ data } = this.props.user[0]);
  };
  onclickEdit = () => {
    // console.log("dada", this.props.user);
    // console.log("asdsa", this.props);
    // console.log("email",this.props.user[0].email);
    this.props.history.push("./editprofile",{user:this.props.user.user.token});
    // console.log("email sus", this.props.user[0].user.email);
    // console.log("dada arr", this.state.imagePath);
    // console.log("name", this.props.user[0].user.firstName);
    // console.log("dada 1234",this.props.user.user.email);
    // console.log("dada ar",this.props.user.user[0].email);
  };

  UNSAFE_componentWillMount(){
    console.log(this.props.user)
    console.log(this.props.user.user)
    console.log(this.props.user.user.token)
      axios.get("https://zenon.onthewifi.com/ticGo/users", {
          headers: {
            Authorization: `Bearer ${this.props.user.user.token}`
          }
        }).then(response => {
          this.setState({
              imagePath: response.data.user.image,
              isLoading: false
          })


      })

  }
  
  state = {
    imagePath:
      "https://firebasestorage.googleapis.com/v0/b/dv-firebase-d6cd3.appspot.com/o/f68896dc-1321-49ee-8cc8-a9055e2d929d.jpg?alt=media"
  };
  selectImage = () => {
    ImagePicker.showImagePicker({}, response => {
      console.log(response);
      if (response.uri) {
        const formData = new FormData();
        formData.append("image", {
          uri: response.uri,
          name: response.fileName,
          type: response.type
        });
        axios
          .post("https://zenon.onthewifi.com/ticGo/users/image", formData, {
            headers: {
              Authorization: `Bearer ${this.props.user.user.token}`
            },
            onUploadProgress: progressEvent => {
              console.log(
                "progress",
                Math.floor((progressEvent.loaded / progressEvent.total) * 100)
              );
            }
          })
          .then(response => {
            this.setState({
              imagePath: response.data.image
            });
          })
          .catch(error => {
            console.log(error.response);
          });
        // this.setState({
        //     imageURI: response.uri
        // })
      }
    });
  };
  render() {
    const { user ,email} = this.props.user
    console.log("user",user.email);
    console.log("user",email);
    console.log("user",user.user);
    return (
     
      <View style={styles.container}>
        <Text style={styles.textProfile}>Profile </Text>
        <View style={styles.imgContainer}>
          <Image source={{ uri: this.state.imagePath }} style={styles.img} />
          <TouchableHighlight onPress={() => this.selectImage()}>
            <Text
              style={{
                textDecorationLine: "underline",
                textAlign: "center",
                marginTop: 5
              }}
            >
              Edit
            </Text>
          </TouchableHighlight>
        </View>

        <View style={styles.text}>
          <Text style={{ margin: 20, fontSize: 16 }}>
            Email : {user.email}
          </Text>
          <Text style={{ margin: 20, fontSize: 16 }}>
            Firstname : {user.firstName}{" "}
          </Text>
          <Text style={{ margin: 20, fontSize: 16 }}>
            Lastname : {user.lastName}
          </Text>
        </View>
        <View style={styles.button}>
          <Button type="warning" onPress={() => this.onclickEdit()}>
            Edit
          </Button>
          <WhiteSpace />
        </View>
        <View>
          <Menu />
        </View>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.userone
  };
};
const styles = StyleSheet.create({
  textProfile: {
    // fontFamily: "Avenir",
    textAlign: "center",
    // fontSize: 24,
    // marginTop: 10,
    backgroundColor: "#1E90FF",
    padding: 15,
    color: "white",
    fontSize: 20,
    textAlign: "center"
  },
  img: {
    width: 100,
    height: 100,
    borderRadius: 50,
    borderWidth: 10
  },
  imgContainer: {
    alignItems: "center",
    marginVertical: 20
  },
  container: {
    flex: 1,
    flexDirection: "column"
  },
  text: {
    marginHorizontal: 30,
    marginVertical: 20
  },
  button: {
    marginHorizontal: 30,
    marginVertical: 24.5
  }
});

export default connect(
  mapStateToProps,
  null
)(profile);
