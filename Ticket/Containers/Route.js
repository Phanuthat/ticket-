import React, { Component } from "react";
import { Route, Switch, Redirect } from "react-router-native";

import Login from "./Login";
import Register from "./Register";
import Movie from "./Movie";
import main from "./Main";
import Profile from "./profile";
import seat from "./seatCinema"
import edit from "./editProfile";
import historyticket from "./historyticket";
import { store, history } from "../Store/store";
import { Provider } from "react-redux";
import { ConnectedRouter } from "connected-react-router";
class route extends Component {
  render() {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <Switch>
            <Route exact path="/Login" component={Login} />
            <Route exact path="/Register" component={Register} />
            <Route exact path="/editprofile" component={edit} />
            <Route exact path="/seat" component={seat}/>
            <Route exact path="/main" component={main} />
            <Route exact path="/profile" component={Profile} />
            <Route exact path="/historyticket" component={historyticket} />

            <Redirect to="/Login" />
          </Switch>
        </ConnectedRouter>
      </Provider>
    );
  }
}

export default route;
