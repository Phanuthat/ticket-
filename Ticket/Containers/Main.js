import React, { Component } from "react";
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  TouchableHighlight
} from "react-native";

import { connect } from "react-redux";

import Modal from "./Modal";
import Movies from "./Movie";
import Menu from "./Menu";

import { Button } from "@ant-design/react-native";
const mapStateToProps = state => {
  return {
    Movies: state.Movies
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onClickMovie: items =>
      dispatch({
        type: "CLICK_MOVIE",
        payload: items
      }),
    onDismissDialog: () =>
      dispatch({
        type: "dismiss_dialog"
      })
  };
};

var screen = Dimensions.get("window");

class main extends Component {
  state = {
    cick: []
  };

  onModalClickCancel = () => {
    this.props.onDismissDialog();
  };
  test1 = () => {
    console.log(this.state.click);
    console.log(this.props);
    JSON.stringify(this.props.Movies.poopae);
  };

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  render() {
    return (
      <View
        style={{ flex: 1, flexDirection: "column",  backgroundColor:"#F8F8FF" }}
      >
        <Movies />
        <Menu />

        <View>
          <Modal />
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  wrapper: {
    paddingTop: 50,
    flex: 1
  },

  modal: {
    justifyContent: "center",
    alignItems: "center"
  },

  modal2: {
    height: 230,
    backgroundColor: "#3B5998"
  },

  modal3: {
    position: "absolute",
    top: 20,
    right: 20,
    left: 20,
    bottom: 20,
    backgroundColor: "rgba(0,0,0,0.6)",
    justifyContent: "center",
    alignItems: "center"
  },

  modal4: {
    height: 600
  },

  btn: {
    margin: 10,
    backgroundColor: "#3B5998",
    color: "white",
    padding: 10
  },

  btnModal: {
    position: "absolute",
    top: 0,
    right: 0,
    width: 50,
    height: 50,
    backgroundColor: "transparent"
  },

  text: {
    color: "black",
    fontSize: 22
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(main);
