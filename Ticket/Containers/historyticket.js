import React, { Component } from "react";
import {
  View,
  Text,
  Button,
  Image,
  StyleSheet,
  TouchableOpacity
} from "react-native";
import { withRouter } from "react-router-native";
import { connect } from "react-redux";
import Menu from "./Menu"
import { Card, WhiteSpace, WingBlank } from "@ant-design/react-native";
import tiket from "../Img/Logo.jpg";
class historyticket extends Component {
  state = {
    movies: []
  };
//  UNSAFE_componentWillMount(){
//    const {time} =this.props.location.state
//    console.log("object", time);
//    console.log(object);
//  }
  render() {
    const { seat, time } = this.props.location.state;
    return (
      <View style={{ backgroundColor: "#F8F8FF" }}>
       <Text style={styles.textProfile}>My Ticket </Text>
        <WingBlank size="lg">
          <View style={{ marginTop: 20 }}>
            <Card>
              <Card.Header
                title={"เรื่อง " + time.time.movie.name}
                thumbStyle={{ width: 30, height: 30 }}
                extra={time.time.movie.duration + "นาที"}
              />
              <Card.Body>
                <View style={{ flexDirection: "row" }}>
                  <View>
                    <WingBlank>
                      <TouchableOpacity>
                        <Image
                          style={{ width: 150, height: 222, borderRadius: 15 }}
                          source={{ uri: time.time.movie.image }}
                        />
                      </TouchableOpacity>
                    </WingBlank>
                  </View>
                  <View>
                    <Text>โรง : {time.time.cinema.name}</Text>
                    <Text>ที่นั่ง : {seat}</Text>
                    <Text>เริ่ม : {new Date(time.time.startDateTime)
                          .toLocaleTimeString()
                          .replace(/(.*)\D\d+/, "$1")}</Text>
                    <Text>จบ : {new Date(time.time.endDateTime)
                          .toLocaleTimeString()
                          .replace(/(.*)\D\d+/, "$1")}</Text>
                    <Text>เสียง : {time.time.soundtrack}</Text>
                    <Text>บรรยาย : {time.time.subtitle}</Text>
                  </View>
                </View>
              </Card.Body>
              <Card.Footer
                content="Tiket"
                
              />
            </Card>
          </View>
        </WingBlank>
        <View style={{marginTop:150}}>
          <Menu />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    // paddingTop: 50,
    flex: 1
  },

  imageThumbnail: {
    justifyContent: "center",
    alignItems: "center",

    height: 150,
    width: 100,
    borderRadius: 15
  },
  textProfile: {
    
    textAlign: "center",
    
    backgroundColor: "#1E90FF",
    padding: 15,
    color: "white",
    fontSize: 20,
    textAlign: "center"
  },
});
const mapStateToProps = state => {
  return {
    user: state.userone
  };
};
export default withRouter(
  connect(
    mapStateToProps,
    null
  )(historyticket)
);
