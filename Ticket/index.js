/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import Login from './Containers/Login'
import profile from './Containers/profile';
import Movie from "./Containers/Movie"
import reg from "./Containers/Register"
import Route from "./Containers/Route"
import modal from "./Containers/Modal"
import historys from "./Containers/historyticket"
import seat from "./Containers/seatCinema"
AppRegistry.registerComponent(appName, () => Route);
