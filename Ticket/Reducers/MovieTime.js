export default (
  state = {
    showTime: [
      {
        cinema: { name: "", seats: [] },
        endDateTime: "",
        movie: {
          name: "",
          image: "",
          duration: 0,
          soundtracks: ["default"],
          subtitles: ["default"],
          id: ""
        },
        seats: [],
        startDateTime: "",
        subtitle: "",
        _id: ""
      }
    ]
  },
  action
) => {
  switch (action.type) {
    case "SHOW_TIME":
      return {
        showTime : action.showTime
      };

    default:
      return state;
  }
};
