export default (
  state = {
    itemMovie: {},
    modalVisible: false,
    poopae: {
      name: "",
      image: "",
      duration: 0,
      soundtracks: ["default"],
      subtitles: ["default"],
      id: ""
    },

  
  },
  action
) => {
  switch (action.type) {
    case "CLICK_MOVIE":
      return {
        modalVisible: true,
        poopae: action.payload
      };
    case "DISMISS_DIALOG":
      return {
        modalVisible: false,
        poopae: []
      };

    default:
      return state;
  }
};
