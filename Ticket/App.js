import React, { Component } from 'react'
import { View, Button, Image } from 'react-native'
import ImagePicker from 'react-native-image-picker'
import axios from 'axios'
 
class App extends Component {
    state = {
        imagePath: ''
    }
    selectImage = () => {
        ImagePicker.showImagePicker({}, (response) => {
            console.log(response)
            if (response.uri) {
                const formData = new FormData()
                formData.append('image', {
                    uri: response.uri,
                    name: response.fileName,
                    type: response.type,
                })
                axios.post('https://zenon.onthewifi.com/ticGo/users/image', formData, {
                    headers: {
                        Authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InRlc3QyQGdtYWlsLmNvbSIsImlkIjoiNWM2ZjZhNTg5NTU3MmIwMDEwMGNjNjM4IiwiZXhwIjoxNTU2MjUwMDAwLCJpYXQiOjE1NTEwNjYwMDB9.EGKQkH4J_6zY5CkYToU7rkuFZL_EwhIaqJT7HS9cR6Y'
                    },
                    onUploadProgress: progressEvent => {
                        console.log('progress', Math.floor(progressEvent.loaded / progressEvent.total * 100))
                    }
                })
                    .then(response => {
                        this.setState({
                            imagePath: response.data.image
                        })
                    })
                    .catch(error => {
                        console.log(error.response)
                    })
                // this.setState({
                //     imageURI: response.uri
                // })
            }
        })
    }
    render() {
        return (
            <View style={{flex: 1}}>
                <Button
                    title="Select Image"
                    onPress={this.selectImage}
                />
                <Image
                    style={{
                        width: '100%',
                        height: 400,
                    }}
                    source={{ uri: this.state.imagePath }}
                />
            </View>
        )
    }
}
 
export default App